#a) #find all artist that has d on its name

	SELECT * FROM artists WHERE name LIKE "%d%";

#b) #find all songs that has a length of 230

	SELECT * FROM songs WHERE length < 230;

#c) join the albums ang songs table (only show the album name, song name, and song length)

	select albums.album_title, songs.song_name, songs.length from albums 
	JOIN songs on albums.id = songs.album_id;


#d) join the artist and albums tables. find all the albums with the letter A in its name

	select * from artists JOIN albums on artists.id = albums.artist_id where albums.album_title LIKE "%a%";


#e) sort the albums table in Z - A order. Show only the first four records.

	select * from albums order by album_title desc limit 4;

#f) join the albums and songs tables. sort album from z - a and sort songs from a - z.

	select * from on albums JOIN songs on albums.id = songs.album_id
	ORDER BY albums.album_title desc, song_name ASC;	